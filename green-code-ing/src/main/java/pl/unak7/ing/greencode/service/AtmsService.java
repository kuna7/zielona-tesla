package pl.unak7.ing.greencode.service;

import pl.unak7.ing.greencode.atmservice.model.ATM;
import pl.unak7.ing.greencode.atmservice.model.Task;
import pl.unak7.ing.greencode.utils.TaskComparator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class AtmsService {

    private final TaskComparator taskComparator = new TaskComparator();

    public List<ATM> process(List<Task> tasks) {
        HashMap<Integer, List<Task>> tasksOrganized = new HashMap<>();

        for(Task task : tasks) {
            if(!tasksOrganized.containsKey(task.getRegion())) {
                tasksOrganized.put(task.getRegion(), new ArrayList<>());
            }
            List<Task> currentRegion = tasksOrganized.get(task.getRegion());
            int taskIndex = currentRegion.indexOf(task);
            if(taskIndex >= 0) {
                if(task.getPriority() > currentRegion.get(taskIndex).getPriority()) {
                    currentRegion.remove(taskIndex);
                    currentRegion.add(task);
                }
            } else {
                currentRegion.add(task);
            }
        }

        List<ATM> result = new ArrayList<>();

        for(int regId : tasksOrganized.keySet().stream().sorted().toList()) {
            result.addAll(tasksOrganized.get(regId).stream().sorted(taskComparator).map(ATM::fromTask).toList());
        }

        return result;
    }
}
