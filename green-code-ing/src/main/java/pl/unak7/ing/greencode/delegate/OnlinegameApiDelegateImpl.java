package pl.unak7.ing.greencode.delegate;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import pl.unak7.ing.greencode.onlinegame.api.OnlinegameApiDelegate;
import pl.unak7.ing.greencode.onlinegame.model.Clan;
import pl.unak7.ing.greencode.onlinegame.model.Players;
import pl.unak7.ing.greencode.service.OnlinegameService;

import java.util.List;

@Component
public class OnlinegameApiDelegateImpl implements OnlinegameApiDelegate {

    OnlinegameService onlinegameService = new OnlinegameService();

    @Override
    public ResponseEntity<List<List<Clan>>> calculate(Players players) {
        return ResponseEntity.ok(onlinegameService.process(players));
    }
}
