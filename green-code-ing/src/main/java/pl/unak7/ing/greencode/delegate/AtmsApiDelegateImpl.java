package pl.unak7.ing.greencode.delegate;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import pl.unak7.ing.greencode.atmservice.api.AtmsApiDelegate;
import pl.unak7.ing.greencode.atmservice.model.ATM;
import pl.unak7.ing.greencode.atmservice.model.Task;
import pl.unak7.ing.greencode.service.AtmsService;

import java.util.List;

@Component
public class AtmsApiDelegateImpl implements AtmsApiDelegate {

    private AtmsService atmsService = new AtmsService();

    @Override
    public ResponseEntity<List<ATM>> calculate(List<Task> tasks) {
        return ResponseEntity.ok(atmsService.process(tasks));
    }
}
