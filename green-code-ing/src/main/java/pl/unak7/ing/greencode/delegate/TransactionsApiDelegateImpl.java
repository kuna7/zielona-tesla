package pl.unak7.ing.greencode.delegate;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import pl.unak7.ing.greencode.service.TransactionsService;
import pl.unak7.ing.greencode.transactions.api.TransactionsApiDelegate;
import pl.unak7.ing.greencode.transactions.model.Account;
import pl.unak7.ing.greencode.transactions.model.Transaction;

import java.util.List;

@Component
public class TransactionsApiDelegateImpl implements TransactionsApiDelegate {

    TransactionsService transactionsService = new TransactionsService();

    @Override
    public ResponseEntity<List<Account>> report(List<Transaction> transactions) {
        return ResponseEntity.ok(transactionsService.process(transactions));
    }
}
