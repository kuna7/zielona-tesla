package pl.unak7.ing.greencode.service;

import pl.unak7.ing.greencode.transactions.model.Account;
import pl.unak7.ing.greencode.transactions.model.Transaction;
import pl.unak7.ing.greencode.utils.AccountComparator;

import java.util.HashMap;
import java.util.List;

public class TransactionsService {

    private final AccountComparator accountComparator = new AccountComparator();

    public List<Account> process(List<Transaction> transactions) {
        final HashMap<String, Account> result = new HashMap<>();

        for (Transaction transaction : transactions) {
            Account debitAccount = result.get(transaction.getDebitAccount());
            if(debitAccount == null) {
                debitAccount = new Account(transaction.getDebitAccount());
                result.put(transaction.getDebitAccount(), debitAccount);
            }

            Account creditAccount = result.get(transaction.getCreditAccount());
            if(creditAccount == null) {
                creditAccount = new Account(transaction.getCreditAccount());
                result.put(transaction.getCreditAccount(), creditAccount);
            }

            debitAccount.debit(transaction.getAmount());
            creditAccount.credit(transaction.getAmount());
        }

        return result.values().stream().sorted(accountComparator).toList();
    }
}
