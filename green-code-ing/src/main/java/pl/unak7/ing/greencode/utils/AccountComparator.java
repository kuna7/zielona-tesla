package pl.unak7.ing.greencode.utils;

import pl.unak7.ing.greencode.transactions.model.Account;

import java.util.Comparator;

public class AccountComparator implements Comparator<Account> {

    private final int ACCOUNT_LENGTH = 26;

    @Override
    public int compare(Account o1, Account o2) {
        for(int i = 0; i < ACCOUNT_LENGTH; i++) {
            int charCompareResult = Character.compare(o1.getAccount().charAt(i), o2.getAccount().charAt(i));
            if(charCompareResult != 0) {
                return charCompareResult;
            }
        }
        return 0;
    }
}
