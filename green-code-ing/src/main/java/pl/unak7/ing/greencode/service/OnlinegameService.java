package pl.unak7.ing.greencode.service;

import pl.unak7.ing.greencode.onlinegame.model.Clan;
import pl.unak7.ing.greencode.onlinegame.model.Players;
import pl.unak7.ing.greencode.utils.ClanComparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class OnlinegameService {

    private final ClanComparator clanComparator = new ClanComparator();

    public List<List<Clan>> process(Players players) {
        List<List<Clan>> result = new ArrayList<>();
        List<Clan> in = players.getClans();

        in.sort(Collections.reverseOrder(clanComparator));

        while(true) {
            if(in.size() == 0) {
                break;
            }
            Clan entryClan = in.remove(0);
            List<Clan> currentEntryClans = new ArrayList<>(Collections.singletonList(entryClan));
            int currentCapacity = players.getGroupCount() - entryClan.getNumberOfPlayers();
            while(true) {
                int indexToRemove = findMatchingClanIndex(in, currentCapacity);
                if(indexToRemove < 0) {
                    break;
                }
                entryClan = in.remove(indexToRemove);
                currentCapacity -= entryClan.getNumberOfPlayers();
                currentEntryClans.add(entryClan);
            }
            result.add(currentEntryClans);
        }

        return result;
    }

    private int findMatchingClanIndex(List<Clan> clans, int capacity) {
        for(int i = 0; i < clans.size(); i++) {
            if(clans.get(i).getNumberOfPlayers() <= capacity) {
                return i;
            }
        }
        return -1;
    }
}
