package pl.unak7.ing.greencode.utils;

import pl.unak7.ing.greencode.atmservice.model.Task;

import java.util.Comparator;

public class TaskComparator implements Comparator<Task> {

    @Override
    public int compare(Task o1, Task o2) {
        int regionCompareResult = Integer.compare(o1.getRegion(), o2.getRegion());
        return regionCompareResult == 0 ? - Byte.compare(o1.getPriority(), o2.getPriority()) : regionCompareResult;
    }
}
