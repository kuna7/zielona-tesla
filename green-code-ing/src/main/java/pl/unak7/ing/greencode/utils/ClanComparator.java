package pl.unak7.ing.greencode.utils;

import pl.unak7.ing.greencode.onlinegame.model.Clan;

import java.util.Comparator;

public class ClanComparator implements Comparator<Clan> {

    @Override
    public int compare(Clan o1, Clan o2) {
        int pointsCompareResult = Integer.compare(o1.getPoints(), o2.getPoints());
        return pointsCompareResult == 0 ? - Integer.compare(o1.getNumberOfPlayers(), o2.getNumberOfPlayers()) : pointsCompareResult;
    }
}
