package pl.unak7.ing.greencode.run;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "pl.unak7.ing.greencode")
public class GreenCodeINGApplication {

	public static void main(String[] args) {
		SpringApplication.run(GreenCodeINGApplication.class, args);
	}

}
