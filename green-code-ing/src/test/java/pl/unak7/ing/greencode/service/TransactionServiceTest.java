package pl.unak7.ing.greencode.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import pl.unak7.ing.greencode.transactions.model.Account;
import pl.unak7.ing.greencode.transactions.model.Transaction;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class TransactionServiceTest {

    private final static int ACCOUNT_SIZE = 26;
    private final TransactionsService toTest = new TransactionsService();
    private final ObjectMapper objectMapper = new ObjectMapper();
    private final TypeReference<List<Transaction>> transactionTypeReference = new TypeReference<List<Transaction>>() {};
    private final TypeReference<List<Account>> accountTypeReference = new TypeReference<List<Account>>() {};

    @Test
    public void exampleTest() throws IOException {
        Resource testRequestResource = new ClassPathResource("transactions/example_request.json");
        List<Transaction> transactions = objectMapper.readValue(testRequestResource.getFile(), transactionTypeReference);
        Resource testResponseResource = new ClassPathResource("transactions/example_response.json");
        List<Account> accounts = objectMapper.readValue(testResponseResource.getFile(), accountTypeReference);
        List<Account> result = toTest.process(transactions);
        Assertions.assertEquals(accounts, result);
    }

    @Test
    public void performanceTest() throws IOException {
        List<Transaction> transactions = generateTransactions(100000, 100000 * 10, -1000000, 1000000);;
        toTest.process(transactions);
    }

    private List<Transaction> generateTransactions(int nTransactions, int nAccounts, double amountMin, double amountMax) {
        Random random = new Random();
        List<Transaction> ret = new ArrayList<>(nTransactions);
        List<String> accounts = new ArrayList<>(nAccounts);
        for (int i = 0; i < nAccounts; i++) {
            accounts.add(generateRandomNumericalString(ACCOUNT_SIZE));
        }
        for(int i = 0; i < nTransactions; i++) {
            ret.add(new Transaction(accounts.get(random.nextInt(nAccounts)),
                    accounts.get(random.nextInt(nAccounts)), random.nextDouble(amountMin, amountMax)));
        }
        return ret;
    }

    private String generateRandomNumericalString(int length) {
        Random random = new Random();
        StringBuilder buffer = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            buffer.append((char) random.nextInt(48,58));
        }
        return buffer.toString();
    }
}
