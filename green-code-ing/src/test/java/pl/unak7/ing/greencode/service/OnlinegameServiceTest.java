package pl.unak7.ing.greencode.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import pl.unak7.ing.greencode.onlinegame.model.Clan;
import pl.unak7.ing.greencode.onlinegame.model.Players;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class OnlinegameServiceTest {

    private final OnlinegameService toTest = new OnlinegameService();
    private final ObjectMapper objectMapper = new ObjectMapper();
    private final TypeReference<Players> playersTypeReference = new TypeReference<Players>() {};
    private final TypeReference<List<List<Clan>>> clansReference = new TypeReference<List<List<Clan>>>() {};

    @Test
    public void exampleTest() throws IOException {
        Resource testRequestResource = new ClassPathResource("onlinegame/example_request.json");
        Players players = objectMapper.readValue(testRequestResource.getFile(), playersTypeReference);
        Resource testResponseResource = new ClassPathResource("onlinegame/example_response.json");
        List<List<Clan>> clans = objectMapper.readValue(testResponseResource.getFile(), clansReference);
        List<List<Clan>> result = toTest.process(players);
        Assertions.assertEquals(clans, result);
    }

    @Test
    public void performanceTest() throws IOException {
        Players players = new Players(1000, generateClans(20000, 1000, 100000));
        toTest.process(players);
    }

    private List<Clan> generateClans(int nClans, int lMax, int pMax) {
        List<Clan> ret = new ArrayList<>(nClans);
        Random random = new Random();
        for(int i =0; i < nClans; i++) {
            ret.add(new Clan(random.nextInt(1, lMax + 1), random.nextInt(1, pMax + 1)));
        }
        return ret;
    }
}
