package pl.unak7.ing.greencode.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import pl.unak7.ing.greencode.atmservice.model.ATM;
import pl.unak7.ing.greencode.atmservice.model.Task;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class AtmsServiceTest {

    private final AtmsService toTest = new AtmsService();
    private final ObjectMapper objectMapper = new ObjectMapper();
    private final TypeReference<List<Task>> taskTypeReference = new TypeReference<List<Task>>() {};
    private final TypeReference<List<ATM>> atmTypeReference = new TypeReference<List<ATM>>() {};

    @Test
    public void exampleTest1() throws IOException {
        Resource testRequestResource = new ClassPathResource("atms/example_1_request.json");
        List<Task> tasks = objectMapper.readValue(testRequestResource.getFile(), taskTypeReference);
        Resource testResponseResource = new ClassPathResource("atms/example_1_response.json");
        List<ATM> atms = objectMapper.readValue(testResponseResource.getFile(), atmTypeReference);
        List<ATM> result = toTest.process(tasks);
        Assertions.assertEquals(atms, result);
    }

    @Test
    public void exampleTest2() throws IOException {
        Resource testRequestResource = new ClassPathResource("atms/example_2_request.json");
        List<Task> tasks = objectMapper.readValue(testRequestResource.getFile(), taskTypeReference);
        Resource testResponseResource = new ClassPathResource("atms/example_2_response.json");
        List<ATM> atms = objectMapper.readValue(testResponseResource.getFile(), atmTypeReference);
        List<ATM> result = toTest.process(tasks);
        Assertions.assertEquals(atms, result);
    }

    @Test
    public void performanceTest() throws IOException {
        List<Task> tasks = generateTasks(10000, 100000);
        toTest.process(tasks);
    }

    private List<Task> generateTasks(int nRegions, int maxAtmsPerRegion) {
        List<Task> ret = new ArrayList<>();
        Random random = new Random();
        for(int i = 0; i < nRegions; i++) {
            for(int j = 0; j < random.nextInt(maxAtmsPerRegion + 1); j++) {
                for(int k =0; k < random.nextInt(4); k++) {
                    ret.add(new Task(i, requestTypeFromPriority(random.nextInt(4)), j));
                }
            }
        }
        return ret;
    }

    private Task.RequestTypeEnum requestTypeFromPriority(int priority) {
        if(priority == 0) {
            return Task.RequestTypeEnum.STANDARD;
        } else if (priority == 1) {
            return Task.RequestTypeEnum.SIGNAL_LOW;
        } else if (priority == 2) {
            return Task.RequestTypeEnum.PRIORITY;
        } else {
            return Task.RequestTypeEnum.FAILURE_RESTART;
        }
    }
}
