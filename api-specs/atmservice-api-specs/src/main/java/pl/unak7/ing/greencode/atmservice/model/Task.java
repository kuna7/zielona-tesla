package pl.unak7.ing.greencode.atmservice.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Objects;

public class Task {

    private int region;
    private RequestTypeEnum requestType;
    private int atmId;
    @JsonIgnore
    private byte priority;

    public enum RequestTypeEnum {
        STANDARD, PRIORITY, SIGNAL_LOW, FAILURE_RESTART;
    }

    public Task() {}

    public Task(int region, RequestTypeEnum requestType, int atmId) {
        this.region = region;
        this.requestType = requestType;
        this.atmId = atmId;
        calculatePriority();
    }

    public int getRegion() {
        return region;
    }

    public void setRegion(int region) {
        this.region = region;
    }

    public RequestTypeEnum getRequestType() {
        return requestType;
    }

    public void setRequestType(RequestTypeEnum requestType) {
        this.requestType = requestType;
        calculatePriority();
    }

    public int getAtmId() {
        return atmId;
    }

    public void setAtmId(int atmId) {
        this.atmId = atmId;
    }

    public byte getPriority() {
        return priority;
    }


    private void calculatePriority() {
        if(requestType == RequestTypeEnum.STANDARD) {
            priority = 1;
        }
        else if(requestType == RequestTypeEnum.SIGNAL_LOW) {
            priority = 2;
        }
        else if(requestType == RequestTypeEnum.PRIORITY) {
            priority = 3;
        }
        else {
            priority = 3;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;
        return region == task.region && atmId == task.atmId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(region, atmId);
    }
}
