package pl.unak7.ing.greencode.transactions.model;

import java.util.Objects;

public class Account {

    private String account;
    private int debitCount = 0;
    private int creditCount = 0;
    private double balance = 0;

    public Account() {}

    public Account(String account) {
        this.account = account;
    }

    public Account(String account, int debitCount, int creditCount, double balance) {
        this.account = account;
        this.debitCount = debitCount;
        this.creditCount = creditCount;
        this.balance = balance;
    }

    public void credit(double amount) {
        this.creditCount++;
        this.balance += amount;
    }

    public void debit(double amount) {
        this.debitCount++;
        this.balance -= amount;
    }

    public String getAccount() {
        return account;
    }

    public int getDebitCount() {
        return debitCount;
    }

    public void setDebitCount(int debitCount) {
        this.debitCount = debitCount;
    }

    public int getCreditCount() {
        return creditCount;
    }

    public void setCreditCount(int creditCount) {
        this.creditCount = creditCount;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account1 = (Account) o;
        return debitCount == account1.debitCount && creditCount == account1.creditCount && Double.compare(account1.balance, balance) == 0 && Objects.equals(account, account1.account);
    }

    @Override
    public int hashCode() {
        return Objects.hash(account, debitCount, creditCount, balance);
    }
}
